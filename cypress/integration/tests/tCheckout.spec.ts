// cypress/support/index.js
/// <reference types="Cypress" />

import { Email } from '../pages/signInEmail';
import { Password } from '../pages/signInPassword';
import { Home } from '../pages/home';
import { Checkout } from '../pages/checkout';

const testData = require("../../fixtures/loginData.json");

describe('Checkout for products in the cart without entering the address', () => {
    const homeObject = new Home();
    const emailObject = new Email();
    const passObject = new Password();
    const checkout = new Checkout();

    beforeEach(() => {
        emailObject.navigate();

    })

    afterEach(() => {
        checkout.leave();
        homeObject.signout();
    })


    testData.forEach((testDataRow: any) => {
        const data = {
            email: testDataRow.email,
            password: testDataRow.password
        };

        context(`Proceed to checkout and continue --> ${data.email} `, () => {
            it("Should open checkout page and then show error message to enter address", () => {
                emailObject.fill_email_box(data.email);
                passObject.fill_password_box(data.password);
                homeObject.cart();
                checkout.proceedToCheckout();
                checkout.checkPage();
                checkout.clickToContinue();
                checkout.CheckEnterAddressMessage();
            });

        });
    });
});

