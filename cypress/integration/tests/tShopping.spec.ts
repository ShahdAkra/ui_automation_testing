// cypress/support/index.js

import { Product } from '../pages/product';
import { Cart } from '../pages/cart';

const searchData = require("../../fixtures/shoppingData.json");


describe("Add new products to the cart for shopping", () => {
  const product = new Product();
  const cart = new Cart();

  searchData.forEach((testDataRow: any) => {
    const data = {
      name: testDataRow.product,
      link: testDataRow.link,
      qty: testDataRow.qty
    };

    context(`Show (${data.name}) `, () => {
      it("Should pass and show selected product", () => {
        product.navigate(data.link);
        product.checkProduct(data.name);
      });
    });

    context(`Add ${data.qty} items from (${data.name}) to cart `, () => {
      it("Should pass and show assertion that it is added ", () => {
        product.addToCart(data.qty);
        product.checkAddingProduct();
        cart.checkNumOfPro(data.qty);
      });
    });

    context(`Open cart and check that ${data.name} is in`, () => {
      it("Should pass ", () => {
        cart.openCart();
        cart.inCartElement(data.name);
      });
    });

  });

});


