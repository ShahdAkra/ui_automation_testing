// cypress/support/index.js
/// <reference types="Cypress" />

import { Email } from '../pages/signInEmail';
import { Password } from '../pages/signInPassword';
import { Home } from '../pages/home';

const testData = require("../../fixtures/loginData.json");
const wrongEmails = require("../../fixtures/wrongEmailData.json");
const wrongPasswords = require("../../fixtures/wrongPasswordData.json");

beforeEach(() => {
    const emailObject = new Email();
    emailObject.navigate();
})

describe('Correct login test', () => {

    afterEach(() => {
        const homeObject = new Home();
        homeObject.signout();
    })

    testData.forEach((testDataRow: any) => {
        const emailObject = new Email();
        const passObject = new Password();
        const data = {
            email: testDataRow.email,
            password: testDataRow.password
        };

        context(`Generating a test for ${data.email} and ${data.password}`, () => {
            it("Login with correct email and password", () => {
                emailObject.fill_email_box(data.email);
                passObject.fill_password_box(data.password);
            });
        });

    });

});

describe('Wrong email test', () => {
    wrongEmails.forEach((wrongEmailsRow: any) => {
        const emailObject = new Email();
        const data = {
            email: wrongEmailsRow.email,
        };

        context(`Generating a test for ${data.email}`, () => {
            it("Login with invalid email ", () => {
                emailObject.fill_email_box(data.email);
                emailObject.check_page();
            });
        });

    });
});

describe('Wrong password test', () => {
    wrongPasswords.forEach((wrongPasswordsRow: any) => {
        const emailObject = new Email();
        const passObject = new Password();
        const data = {
            email: wrongPasswordsRow.email,
            password: wrongPasswordsRow.password
        };

        context(`Generating a test for ${data.email} and ${data.password}`, () => {
            it("Login with wrong password ", () => {
                emailObject.fill_email_box(data.email);
                passObject.fill_password_box(data.password);
                passObject.check_page();
            });
        });

    });
});
