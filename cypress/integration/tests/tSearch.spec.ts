// cypress/support/index.js

import { Home } from '../pages/home';
import { Result } from '../pages/result';
const searchData = require("../../fixtures/searchData.json");
const wSearchData = require("../../fixtures/wrongSearchData.json");

const search = new Home();

describe("Search in specified categories for some items and sort it as required", () => {
  const result = new Result();
  before(() => {
    search.navigate();
  })
  searchData.forEach((testDataRow: any) => {
    const data = {
      category: testDataRow.category,
      term: testDataRow.term,
      sortBy: testDataRow.sortBy
    };

    context(`Search in ${data.category} for ${data.term}`, () => {
      it("Should pass and show search items", () => {
        search.search(data.term, data.category);
        search.checkResult();
      });
    });
    context(`Sort ${data.term} products by ${data.sortBy}`, () => {
      it("Should pass and show correct sorted items", () => {
        result.sort(data.sortBy);
      });
    });
  });
});

describe("Search in specified category for unrelated product (Wrong search)", () => {

  wSearchData.forEach((testDataRow: any) => {
    const wData = {
      category: testDataRow.category,
      term: testDataRow.term
    };
    context(`Search in ${wData.category} for ${wData.term}`, () => {
      it("Should pass and show message of no result", () => {
        search.search(wData.term, wData.category);
        search.checkNoResultMessage();
      });
    });
  });

});

