// cypress/support/index.js
/// <reference types="Cypress" />


export class Product {

    navigate(term: string) {
        cy.visit(term);
        return this;
    }

    checkProduct(data: string) {
        cy.get('span[id=productTitle]').should(($title) => {
            const text = $title.text();
            expect(text).to.include(data);
        })
        return this;
    }

    addToCart(qty: string) {
        cy.get('[name=quantity]').scrollIntoView().select(qty, { force: true });
        cy.get('input[id="add-to-cart-button"]').click();
    }

    checkAddingProduct() {
        cy.get('h1[class="a-size-medium a-text-bold"]').should(($el) => {
            expect($el).to.have.text('\n      Added to Cart\n    ');
        })
    }


}