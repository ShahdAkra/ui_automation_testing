// cypress/support/index.js
/// <reference types="Cypress" />

export class Result {

    checkResult() {
        cy.get('[data-component-type=s-search-result]').should('exist');
        return this;
    }

    sort(by: string) {
        cy.get('select[name=s]').select(by, { force: true });
        cy.wait(3000);
        return this;
    }

    noResultMessage() {
        cy.get('[class="a-size-medium a-color-base"]').first().should(($span) => {
            expect($span).to.contain('No results for');
        })
        return this;
    }

}