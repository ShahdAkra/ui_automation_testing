// cypress/support/index.js
/// <reference types="Cypress" />


export class Password {

    fill_password_box(term: String) {
        const field = cy.get('[name=password]');
        field.type(`${term}{enter}`);
        return this;
    }

    check_page() {
        cy.wait(300);
        cy.location().should((loc) => {
            expect(loc.href).to.eq('https://www.amazon.com/ap/signin')
        })
        return this;
    }

}