// cypress/support/index.js
/// <reference types="Cypress" />

export class Checkout {

    proceedToCheckout() {
        cy.wait(3000);
        cy.get('[name="proceedToRetailCheckout"]').click({ force: true });
        return this;
    }

    checkPage() {
        cy.get('h1[class="a-spacing-base"]').should(($h1) => {
            expect($h1).to.contain('Select a shipping address');
        })
        return this;
    }

    clickToContinue() {
        cy.get('[name="shipToThisAddress"]').click({ force: true });
        return this;
    }

    CheckEnterAddressMessage() {
        cy.get('[id="addressIMB"]').should(($div) => {
            expect($div).to.exist;
        })
        return this;
    }

    leave() {
        cy.go('back');
    }

}