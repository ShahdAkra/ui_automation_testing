// cypress/support/index.js
/// <reference types="Cypress" />


export class Email {

    navigate() {
        cy.visit('https://amazon.com');
        cy.get('#nav-link-accountList').click(130.53, 50, { force: true });
        return this;
    }

    fill_email_box(term: String) {
        const field = cy.get('[name=email]');
        field.type(`${term}{enter}`);
        return this;
    }

    check_page() {
        cy.get('label').contains('Email (phone for mobile accounts)');
        return this;
    }

}