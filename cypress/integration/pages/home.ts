// cypress/support/index.js
/// <reference types="Cypress" />

import { Result } from '../pages/result';

export class Home {

    navigate() {
        cy.visit('https://www.amazon.com');
        return this;
    }

    search(term: string, category: string) {
        cy.get('[name=url]').select(category, { force: true });
        const field = cy.get('[name=field-keywords]');
        field.type(`{selectall}{del}${term}{enter}`);
        return this;
    }

    checkResult() {
        return new Result().checkResult();
    }

    cart() {
        cy.get('[id="nav-cart"]').click();
        return this;
    }

    numberOfAddedPro(data: number) {
        cy.get('[id="nav-cart-count"]').contains(data);
        return this;
    }

    checkNoResultMessage() {
        return new Result().noResultMessage();
    }

    signout() {
        cy.get('[id=nav-item-signout]').click({ force: true });
        return this;
    }

}