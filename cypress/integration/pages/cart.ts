// cypress/support/index.js
/// <reference types="Cypress" />
import { Home } from '../pages/home';

export class Cart {

    openCart() {
        return new Home().cart();
    }

    checkNumOfPro(data: number) {
        return new Home().numberOfAddedPro(data);
    }

    inCartElement(data:string) {
        cy.get('[class="a-size-medium sc-product-title a-text-bold"]').should(($title) => {
            const text = $title.text();
            expect(text).to.include(data);
        })
    }

}